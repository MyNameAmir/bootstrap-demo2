
import validateCar from '../../hooks/validate-car';
export default {
  before: {
    all: [],
    find: [],
    get: [],
    create: [validateCar()],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
